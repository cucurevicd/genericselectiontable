//
//  TextInputTableViewCell.swift
//  TestGenericTable
//
//  Created by Apple on 2/1/19.
//  Copyright © 2019 Quantox. All rights reserved.
//

import UIKit

let kCellInputTextNotificaiton = "kCellInputTextNotificaiton"

class TextInputTableViewCell: BaseTableViewCell, UITextFieldDelegate {
    
    override func prepareForReuse() {
        self.textInput.text = ""
    }
    
    @IBOutlet weak var textInput: UITextField!
    
    //MARK:- Override superclass methods
    override func config(data: CellModelProtocol?) {
        self.textInput.placeholder = data?.getTitle()
        
        self.textInput.delegate = self
    }
    
    override func cellIsSelected(isSelected: Bool) {
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kCellInputTextNotificaiton), object: self, userInfo: ["inputText": textField.text!])
        self.setModelSelected(isSelected: !textField.text!.isEmpty)
        textField.resignFirstResponder()
        return true
    }
    
}

