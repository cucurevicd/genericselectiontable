//
//  MultipleSelectionAndAccordionWithSingleSelectionVC.swift
//  TestGenericTable
//
//  Created by Apple on 2/1/19.
//  Copyright © 2019 Quantox. All rights reserved.
//

import UIKit

class MultipleSelectionAndAccordionWithSingleSelectionVC: UIViewController {

    var embeded: GenericTableController? {
        return self.children.filter({ (value) -> Bool in return value.isKind(of: GenericTableController.self)}).first as? GenericTableController
    }
    
    //MARK:- Models
    var basicCell1: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Hello 1")
    }
    var basicCell2: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Hello 2")
    }
    var basicCell3: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Hello 3")
    }
    var subtitleBasicCell1 : NativeBasicSubtitleCell{
        return NativeBasicSubtitleCell(title: "Subtitle 1", subtitle: "Hello subtitle")
    }
    var subtitleBasicCell2 : NativeBasicSubtitleCell{
        return NativeBasicSubtitleCell(title: "Subtitle 2", subtitle: "Hello subtitle")
    }
    
    var accordionModel1: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Accordion1")
    }
    var accordionModel2: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Accordion2")
    }
    
    //MARK:- Cells
    lazy var cell1 = CellModel(identifier: "basicCell", model: basicCell1, type: .selection, accessoryType: .checkmark)
    lazy var cell2 = CellModel(identifier: "basicSubtitleCell", model: subtitleBasicCell1, type: .selection, accessoryType: .checkmark)
    
    lazy var cell3 = CellModel(identifier: "basicCell", model: basicCell2, type: .accordion(true), accessoryType: .checkmark)
    
    lazy var cell4 = CellModel(identifier: "basicCell", model: basicCell3, type: .selection, accessoryType: .checkmark)
    lazy var cell5 = CellModel(identifier: "basicSubtitleCell", model: subtitleBasicCell2, type: .selection, accessoryType: .checkmark)
    
    func getCells() -> [CellModel]{
        return [cell1, cell2, cell3, cell4, cell5]
    }
    
    lazy var additionalCells : [CellModel] = {
        //Subaccordion cell must have parent!
        let cell1 = CellModel(identifier: "SubAccordionTableViewCell", model: accordionModel1, type: .selection, isCellSelected: false, accessoryType: .checkmark, parent: cell3)
        let cell2 = CellModel(identifier: "SubAccordionTableViewCell", model: accordionModel2, type: .selection,isCellSelected: false, accessoryType: .checkmark, parent: cell3)
        return [cell1, cell2]
    }()
    
    //MARK:- View cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configTable()
    }
    
    //MARK:- Table config
    private func configTable(){
        
        embeded?.tableView.accordionSelection = { [unowned self] (model, indexPath) in
            print("Accordion add:")
            print("IndexPath:\(indexPath)")
            self.embeded?.tableView.insertCells(models: self.additionalCells,at: indexPath)
        }
        
        embeded?.tableView.accordionDeselection = { [unowned self] (model, indexPath) in
            print("Accordion remove:")
            print("IndexPath:\(indexPath)")
            self.embeded?.tableView.deleteCells(models: self.additionalCells)
        }
        
        embeded!.tableView.config(data: getCells(), isMultipleSelection: true) { (selectedCell, selectedIndexPaths, lastSelected) in
            
            print("Selected titles: \(selectedCell.map{$0.model?.getTitle()})")
            
            // Remove previously selected cell in accordion sub types
            if let lastSelection = lastSelected{
                if let parent = lastSelection.parentModel{
                    let newSelection = selectedCell.filter{ $0 != lastSelection }
                    let deselectModels = newSelection.filter{ $0.parentModel?.first == parent.first && $0 != lastSelection }
                    self.embeded?.tableView.deselectCells(models: deselectModels)
                }
            }
        }
        
        // Always call after tableView.config()
        embeded?.tableView.setSelectedCells()
    }

}
