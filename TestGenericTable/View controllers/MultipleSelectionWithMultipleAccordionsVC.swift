//
//  MultipleSelectionWithMultipleAccordionsVC.swift
//  TestGenericTable
//
//  Created by Apple on 2/1/19.
//  Copyright © 2019 Quantox. All rights reserved.
//

import UIKit

class MultipleSelectionWithMultipleAccordionsVC: UIViewController {

    var embeded: GenericTableController? {
        return self.children.filter({ (value) -> Bool in return value.isKind(of: GenericTableController.self)}).first as? GenericTableController
    }
    
    //MARK:- Models
    var basicCell1: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Hello 1")
    }
    
    var accordionCell1: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Accordion 1")
    }
    
    var accordionCell2: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Accordion 2")
    }
    
    var basicCell2: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Accordion")
    }
    var basicCell3: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Hello 3")
    }
    var subtitleBasicCell1 : NativeBasicSubtitleCell{
        return NativeBasicSubtitleCell(title: "Subtitle 1", subtitle: "Hello subtitle")
    }
    var subtitleBasicCell2 : NativeBasicSubtitleCell{
        return NativeBasicSubtitleCell(title: "Subtitle 2", subtitle: "Hello subtitle")
    }
    
    var accordionModel1: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Accordion1")
    }
    var accordionModel2: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Accordion2")
    }
    var accordionModel3: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Accordion3")
    }
    var accordionModel4: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Accordion4")
    }
    
    //MARK:- Cells
    lazy var cell1 = CellModel(identifier: "basicCell", model: basicCell1, type: .selection, accessoryType: .checkmark)
    lazy var cell2 = CellModel(identifier: "basicSubtitleCell", model: subtitleBasicCell1, type: .selection, accessoryType: .checkmark)
    
    lazy var cell3 = CellModel(identifier: "basicCell", model: accordionCell1, type: .accordion(false), accessoryType: .checkmark)
    
    lazy var cell4 = CellModel(identifier: "basicCell", model: basicCell3, type: .selection, accessoryType: .checkmark)
    
    lazy var cell5 = CellModel(identifier: "basicCell", model: accordionCell2, type: .accordion(false), accessoryType: .checkmark)
    
    func getCells() -> [CellModel]{
        return [cell1, cell2, cell3, cell4, cell5]
    }
    
    lazy var additionalCellsAccordion1 : [CellModel] = {
        //Subaccordion cell must have parent!
        let cell1 = CellModel(identifier: "SubAccordionTableViewCell", model: accordionModel1, type: .selection, isCellSelected: false, accessoryType: .checkmark, parent: cell3)
        let cell2 = CellModel(identifier: "SubAccordionTableViewCell", model: accordionModel2, type: .selection,isCellSelected: false, accessoryType: .checkmark, parent: cell3)
        return [cell1, cell2]
    }()
    
    lazy var additionalCellsAccordion2 : [CellModel] = {
        //Subaccordion cell must have parent!
        let cell1 = CellModel(identifier: "SubAccordionTableViewCell", model: accordionModel3, type: .selection, isCellSelected: false, accessoryType: .checkmark, parent: cell5)
        let cell2 = CellModel(identifier: "SubAccordionTableViewCell", model: accordionModel4, type: .selection,isCellSelected: false, accessoryType: .checkmark, parent: cell5)
        return [cell1, cell2]
    }()
    
    //MARK:- View cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configTable()
    }
    
    //MARK:- Table config
    private func configTable(){
        
        embeded?.tableView.accordionSelection = { [unowned self] (model, indexPath) in
            print("Accordion add:")
            print("IndexPath:\(indexPath)")
            
            // Add specific cells based on model subModels (subaccordion cells)
            var cells = [CellModel]()
            if model == self.cell3{
                cells = self.additionalCellsAccordion1
            }
            else if model == self.cell5{
                cells = self.additionalCellsAccordion2
            }
            self.embeded?.tableView.insertCells(models: cells,at: indexPath)
        }
        
        embeded?.tableView.accordionDeselection = { [unowned self] (model, indexPath) in
            print("Accordion remove:")
            print("IndexPath:\(indexPath)")
            
            // Remove specific cells based on model subModels (subaccordion cells)
            var cells = [CellModel]()
            if model == self.cell3{
                cells = self.additionalCellsAccordion1
            }
            else if model == self.cell5{
                cells = self.additionalCellsAccordion2
            }
            self.embeded?.tableView.deleteCells(models: cells)
        }
        
        embeded!.tableView.config(data: getCells(), isMultipleSelection: true) { [unowned self] (selectedCell, selectedIndexPaths, lastSelected) in
            
            print("Selected titles: \(selectedCell.map{$0.model?.getTitle()})")
            
            // We want first accordion to be multiple selection
            if let lastSelection = lastSelected{
                if let parentCellModel = lastSelection.parentModel{
                    
                    // Filter by CellModel:
                    let singleSelectionCells : [CellModel] = [self.cell5]
                    if let parentCell = parentCellModel.first, singleSelectionCells.contains(parentCell){
                        let newSelection = selectedCell.filter{ $0 != lastSelection }
                        let deselectModels = newSelection.filter{ $0.parentModel?.first == parentCell && $0 != lastSelection}
                        self.embeded?.tableView.deselectCells(models: deselectModels)
                    }
                    
                    // Filter by CellModelProtocol
//                    let singleSelectionAccordionsModels : [CellModelProtocol] = [self.accordionCell2]
//                    let newSelection = selectedCell.filter{ $0 != lastSelection }
//                    let deselectModels = newSelection.filter{ value in
//                        value.parentModel?.first == parentModel.first &&
//                            value != lastSelection &&
//                            singleSelectionAccordionsModels.contains(where: {$0.getUniqueID() == parentModel.first?.model?.getUniqueID()})
//                    }
//                    self.embeded?.tableView.deselectCells(models: deselectModels)
                }
            }
        }
        
        // Always call after tableView.config()
        embeded?.tableView.setSelectedCells()
    }

}
