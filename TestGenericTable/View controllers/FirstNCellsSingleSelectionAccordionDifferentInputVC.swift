//
//  FirstNCellsSingleSelectionAccordionDifferentInputVC.swift
//  TestGenericTable
//
//  Created by Apple on 2/1/19.
//  Copyright © 2019 Quantox. All rights reserved.
//

import UIKit

class FirstNCellsSingleSelectionAccordionDifferentInputVC: UIViewController {


    var embeded: GenericTableController? {
        return self.children.filter({ (value) -> Bool in return value.isKind(of: GenericTableController.self)}).first as? GenericTableController
    }
    
    //MARK:- Models
    var basicCell1: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Hello 1")
    }
    var basicCell2: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Hello 2")
    }
    var basicCell3: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Hello 3")
    }
    
    var basicCell4: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Accordion -- click here")
    }
    
    var accordionModel1: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Text input")
    }
    
    var accordionModel2: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Accordion2")
    }
    
    
    //MARK:- Cells
    lazy var cell1 = CellModel(identifier: "basicCell", model: basicCell1, type: .selection, accessoryType: .checkmark)
    lazy var cell2 = CellModel(identifier: "basicCell", model: basicCell2, type: .selection, accessoryType: .checkmark)
    lazy var cell3 = CellModel(identifier: "basicCell", model: basicCell3, type: .selection, accessoryType: .checkmark)
    
    //Accordio set as cellModel type *accordion(can_accordion_be_selected)
    lazy var cell4 = CellModel(identifier: "basicCell", model: basicCell4, type: .accordion(false), accessoryType: .none)
    
    func getCells() -> [CellModel]{
        return [cell1, cell2, cell3, cell4]
    }
    
    lazy var additionalCells : [CellModel] = {
        //Subaccordion cell must have parent!
        
        // Type .noSelection is important because we do not need to have those cells selected, and we do not want to deselect other cells
        let cell1 = CellModel(identifier: "TextInputTableViewCell", model: accordionModel1, type: .noSelection, accessoryType: .none, parent: cell4)
        let cell2 = CellModel(identifier: "SwitchTableViewCell", model: accordionModel1, type: .noSelection, accessoryType: .none, parent: cell4)
        return [cell1, cell2]
    }()
    
    //MARK:- View cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configTable()
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: kCellInputTextNotificaiton), object: nil, queue: OperationQueue.current) { (notification) in
            
            if let userInfo = notification.userInfo{
                print("Text: \(userInfo["inputText"] ?? "")")
            }
        }
    }
    
    //MARK:- Table config
    private func configTable(){
        
        // Accordion selection
        embeded?.tableView.accordionSelection = { [unowned self] (model, indexPath) in
            print("Accordion add:")
            print("IndexPath:\(indexPath)")
            self.embeded?.tableView.insertCells(models: self.additionalCells,at: indexPath)
        }
        
        // Accordion deselection
        embeded?.tableView.accordionDeselection = { [unowned self] (model, indexPath) in
            print("Accordion remove:")
            print("IndexPath:\(indexPath)")
            self.embeded?.tableView.deleteCells(models: self.additionalCells)
        }
        
        embeded!.tableView.config(data: getCells(), isMultipleSelection: true) { [unowned self] (selectedCell, selectedIndexPaths, lastSelected) in
            
            if let lastSelectedModel = lastSelected,!self.additionalCells.contains(lastSelectedModel){
                let deselect = selectedCell.filter{ $0 != lastSelected }.filter{ !self.additionalCells.contains($0) }
                self.embeded?.tableView.deselectCells(models: deselect)
            }
            print("Selected titles: \(selectedCell.map{$0.model?.getTitle()})")
        }
        
        // Always call after tableView.config()
        embeded?.tableView.setSelectedCells()
    }

}
