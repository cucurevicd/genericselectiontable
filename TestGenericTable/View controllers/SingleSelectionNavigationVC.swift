//
//  SingleSelectionNavigationVC.swift
//  TestGenericTable
//
//  Created by Apple on 2/1/19.
//  Copyright © 2019 Quantox. All rights reserved.
//

import UIKit

class SingleSelectionNavigationVC: UIViewController {
    
    var embeded: GenericTableController? {
        return self.children.filter({ (value) -> Bool in return value.isKind(of: GenericTableController.self)}).first as? GenericTableController
    }
    
    //MARK:- Models
    var basicCell1: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Hello 1")
    }
    var basicCell2: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Hello 2")
    }
    var basicCell3: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Hello 3")
    }
    var subtitleBasicCell1 : NativeBasicSubtitleCell{
        return NativeBasicSubtitleCell(title: "Subtitle 1", subtitle: "Hello subtitle")
    }
    var subtitleBasicCell2 : NativeBasicSubtitleCell{
        return NativeBasicSubtitleCell(title: "Subtitle 2", subtitle: "Hello subtitle")
    }
    
    //MARK:- Cells
    lazy var cell1 = CellModel(identifier: "basicCell", model: basicCell1, type: .selection, isCellSelected: true, accessoryType: .checkmark)
    lazy var cell2 = CellModel(identifier: "basicSubtitleCell", model: subtitleBasicCell1, type: .selection, accessoryType: .checkmark)
    lazy var cell3 = CellModel(identifier: "basicCell", model: basicCell2, type: .selection,isCellSelected: false, accessoryType: .checkmark)
    lazy var cell4 = CellModel(identifier: "basicCell", model: basicCell3, type: .selection, accessoryType: .checkmark)
    lazy var cell5 = CellModel(identifier: "basicSubtitleCell", model: subtitleBasicCell2, type: .navigation, accessoryType: .disclosureIndicator)

    func getCells() -> [CellModel]{
        return [cell1, cell2, cell3, cell4, cell5]
    }
    
    //MARK:- View cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        configTable()
    }
    
    //MARK:- Table config
    private func configTable(){
        
        embeded?.tableView.navigationSelection = {[unowned self] (model, indexPath) in
            self.performSegue(withIdentifier: "secondViewController", sender: nil)
        }

        embeded!.tableView.config(data: getCells(), isMultipleSelection: false) { (selectedCells, selectedIndexPaths, lastSelected) in
            print("Selected titles: \(selectedCells.map{$0.model?.getTitle()})")
        }
        
        // Always call after tableView.config()
        embeded?.tableView.setSelectedCells()
    }

}
