//
//  MultipleSelectionWithUISpecificCellVC.swift
//  TestGenericTable
//
//  Created by Apple on 2/1/19.
//  Copyright © 2019 Quantox. All rights reserved.
//

import UIKit

class MultipleSelectionWithUISpecificCellVC: UIViewController {

    var embeded: GenericTableController? {
        return self.children.filter({ (value) -> Bool in return value.isKind(of: GenericTableController.self)}).first as? GenericTableController
    }
    
    //MARK:- Models
    var basicCell1: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Hello 1")
    }
    var basicCell2: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Hello 2")
    }
    var basicCell3: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Hello 3")
    }
    var subtitleBasicCell1 : NativeBasicSubtitleCell{
        return NativeBasicSubtitleCell(title: "Subtitle 1", subtitle: "Hello subtitle")
    }
    var subtitleBasicCell2 : NativeBasicSubtitleCell{
        return NativeBasicSubtitleCell(title: "Specific UI elment", subtitle: "Hello subtitle")
    }
    
    //MARK:- Cells
    lazy var cell1 = CellModel(identifier: "basicCell", model: basicCell1, type: .selection, accessoryType: .checkmark)
    lazy var cell2 = CellModel(identifier: "basicSubtitleCell", model: subtitleBasicCell1, type: .selection, accessoryType: .checkmark)
    lazy var cell3 = CellModel(identifier: "basicCell", model: basicCell2, type: .selection, accessoryType: .checkmark)
    lazy var cell4 = CellModel(identifier: "basicCell", model: basicCell3, type: .selection, accessoryType: .checkmark)
    
    // Important for specific ui elements where you want to disable cell selection is to set type as "noSelection"
    lazy var cell5 = CellModel(identifier: "SwitchTableViewCell", model: subtitleBasicCell2, type: .noSelection, accessoryType: .none)
    
    func getCells() -> [CellModel]{
        return [cell1, cell2, cell3, cell4, cell5]
    }
    
    //MARK:- View cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configTable()
    }
    
    //MARK:- Table config
    private func configTable(){
        embeded!.tableView.config(data: getCells(), isMultipleSelection: true) { (selectedCell, selectedIndexPaths, lastSelected) in
            print("Selected titles: \(selectedCell.map{$0.model?.getTitle()})")
        }
        
        // Always call after tableView.config()
        embeded?.tableView.setSelectedCells()
    }

}
