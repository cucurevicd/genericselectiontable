//
//  SingleSelectionAccordionSwitchVC.swift
//  TestGenericTable
//
//  Created by Apple on 2/1/19.
//  Copyright © 2019 Quantox. All rights reserved.
//

import UIKit

class SingleSelectionAccordionSwitchVC: UIViewController {

    var embeded: GenericTableController? {
        return self.children.filter({ (value) -> Bool in return value.isKind(of: GenericTableController.self)}).first as? GenericTableController
    }
    
    //MARK:- Models
    var basicCell1: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Hello 1")
    }
    var basicCell2: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Hello 2")
    }
    var basicCell3: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Hello 3")
    }
    var subtitleBasicCell1 : NativeBasicSubtitleCell{
        return NativeBasicSubtitleCell(title: "Subtitle 1", subtitle: "Hello subtitle")
    }
    var subtitleBasicCell2 : NativeBasicSubtitleCell{
        return NativeBasicSubtitleCell(title: "Subtitle 2", subtitle: "Hello subtitle")
    }
    var accordionModel1: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Accordion1")
    }
    
    var accordionModel2: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Accordion2")
    }
    
    //MARK:- Cells
    lazy var cell1 = CellModel(identifier: "basicCell", model: basicCell1, type: .selection, accessoryType: .checkmark)
    lazy var cell2 = CellModel(identifier: "basicSubtitleCell", model: subtitleBasicCell1, type: .selection, accessoryType: .checkmark)
    
    //Accordio set as cellModel type *accordion(can_accordion_be_selected)
    lazy var cell3 = CellModel(identifier: "basicCell", model: basicCell2, type: .accordion(false), accessoryType: .none)
    lazy var cell4 = CellModel(identifier: "basicCell", model: basicCell3, type: .selection, accessoryType: .checkmark)
    lazy var cell5 = CellModel(identifier: "basicSubtitleCell", model: subtitleBasicCell2, type: .selection, accessoryType: .checkmark)
    
    func getCells() -> [CellModel]{
        return [cell1, cell2, cell3, cell4, cell5]
    }
    
    lazy var additionalCells : [CellModel] = {
        //Subaccordion cell must have parent!
        
        //IMPORTANT: To use single selection logic of table view, type must be set to "selection"!!!
        let cell1 = CellModel(identifier: "SwitchTableViewCell", model: accordionModel1, type: .selection, accessoryType: .none, parent: cell3)
        return [cell1]
    }()
    
    //MARK:- View cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configTable()
    }
    
    //MARK:- Table config
    private func configTable(){
        
        // Accordion selection
        embeded?.tableView.accordionSelection = { [unowned self] (model, indexPath) in
            print("Accordion add:")
            print("IndexPath:\(indexPath)")
            
            self.embeded?.tableView.insertCells(models: self.additionalCells,at: indexPath)
        }
        
        // Accordion deselection
        embeded?.tableView.accordionDeselection = { [unowned self] (model, indexPath) in
            print("Accordion remove:")
            print("IndexPath:\(indexPath)")
            self.embeded?.tableView.deleteCells(models: self.additionalCells)
        }
        
        embeded!.tableView.config(data: getCells(), isMultipleSelection: true) { (selectedCell, selectedIndexPaths, lastSelected) in
            print("Selected titles: \(selectedCell.map{$0.model?.getTitle()})")
        }
        
        // Always call after tableView.config()
        embeded?.tableView.setSelectedCells()
    }
}
