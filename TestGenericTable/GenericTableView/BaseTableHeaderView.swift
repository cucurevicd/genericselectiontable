//
//  BaseTableHeaderView.swift
//  TestGenericTable
//
//  Created by Apple on 1/31/19.
//  Copyright © 2019 Quantox. All rights reserved.
//

import UIKit

class BaseTableHeaderView: UITableViewHeaderFooterView {

    func config(model: SectionModel){
        fatalError("Must override")
    }
}
