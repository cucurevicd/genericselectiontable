//
//  GenericTableView.swift
//  modelhubapp_ios
//
//  Created by admin on 12/12/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

struct SectionModel{
    
    let title: String?
    var cells: [CellModel]?
    var isMultipleSelection: Bool = false
    let headerHeight : CGFloat? = nil
    let customHeaderXibIdentifier: String? = nil
}

protocol CellModelProtocol  {
    
    func getTitle()->String
    func getSubtitle()->String
    func getUniqueID()->String
}

enum CellModelType: Equatable{
    case navigation
    case selection
    case accordion(Bool)
    case noSelection
    
    var selectable : Bool{
        get { return [CellModelType.navigation,CellModelType.selection, CellModelType.accordion(true)].contains(self)}
    }
    
    var isAccordion: Bool{
        get{ return [CellModelType.accordion(true), CellModelType.accordion(false)].contains(self)}
    }
}

struct NativeBasicTableCell : CellModelProtocol{
    
    var title : String?
    
    init(title: String) {
        self.title = title
    }
    
    func getTitle() -> String {
        return title ?? ""
    }
    
    func getSubtitle() -> String {
        return ""
    }
    
    func getUniqueID() -> String {
        return title ?? ""
    }
}

struct NativeBasicSubtitleCell : CellModelProtocol{
    
    var title : String?
    var subtitle: String?
    
    init(title: String, subtitle: String) {
        self.title = title
        self.subtitle = subtitle
    }
    
    func getTitle() -> String {
        return title ?? ""
    }
    
    func getSubtitle() -> String {
        return subtitle ?? ""
    }
    
    func getUniqueID() -> String {
        return title ?? ""
    }
}

struct CellModel : Equatable{
    
    static func == (lhs: CellModel, rhs: CellModel) -> Bool {
        return lhs.model?.getUniqueID() == rhs.model?.getUniqueID()
    }
    
    var isCellSelected:Bool = false
    var identifier:String?
    var model:CellModelProtocol?
    var type : CellModelType = .selection
    var accesoryType: UITableViewCell.AccessoryType = .none
    var parentModel : [CellModel]? = nil
    
    init(identifier:String, model:CellModelProtocol, type: CellModelType, isCellSelected:Bool = false, accessoryType : UITableViewCell.AccessoryType = .none, parent: CellModel? = nil){
        self.identifier = identifier
        self.model = model
        self.isCellSelected = isCellSelected
        self.type = type
        self.accesoryType = accessoryType
        
        if let parent = parent{
            self.parentModel = [parent]
        }
    }
}
typealias SelectedData = (_ selectedModels:[CellModel], _ indexPaths : [IndexPath]?,_ lastSelected: CellModel?)->Void


// Novo
typealias GenericIndexPath = (_ indexPath: IndexPath) -> Void
typealias AccordionSelection = (_ model: CellModel,_ indexPath: IndexPath) -> Void
typealias NavigationSelection = (_ model: CellModel,_ indexPath: IndexPath) -> Void

class GenericTableView:UITableView, UITableViewDelegate, UITableViewDataSource{
    
    //MARK:- Vars
    var sections: [SectionModel]?
    var model:[CellModel] = []
    var openedAccordionModels = [CellModel]()

    // MARK:- Callbacks
    var selectedData:SelectedData?
    
    var selectedIndexPath:GenericIndexPath?
    var deselectedIndexPath:GenericIndexPath?
    
    var accordionSelection: AccordionSelection?
    var accordionDeselection : AccordionSelection?
    
    var navigationSelection: NavigationSelection?
    
    //MARK:- Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.delegate = self
        self.dataSource = self
    }
    
    //MARK:- Table config
    func config(data:[CellModel], isMultipleSelection:Bool?, completion:@escaping SelectedData){
        model = data
        self.allowsMultipleSelection = isMultipleSelection ?? false
        self.selectedData = completion
        
        openAccordionCellOnStart()
    }
    
    func config(withSections sections: [SectionModel], completion: @escaping SelectedData){
        self.sections = sections
        self.allowsMultipleSelection = true
        self.selectedData = completion
        
        openAccordionCellOnStart()
    }
    
    //MARK:- Data source
    func numberOfSections(in tableView: UITableView) -> Int {
         return sections?.count ?? 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections?[section].cells?.count ?? model.count
    }
    
    //MARK:- Config cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cellModel = getModel(for: indexPath) else { return UITableViewCell () }
        guard let cellIdentifier = cellModel.identifier else{ fatalError("Your cell model do not have cell identifier") }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        
        // Custom tableView cell
        if let baseCell = cell as? BaseTableViewCell{
            baseCell.config(data: cellModel.model)
            baseCell.delegate = self
            
            // Accordion opne indcator
            if cellModel.type.isAccordion{
                baseCell.setAccordionStatus(open: openedAccordionModels.contains(cellModel))
            }
        }
        // Native UITableViewCell
        else{
            cell.textLabel?.text = cellModel.model?.getTitle()
            cell.detailTextLabel?.text = cellModel.model?.getSubtitle()
        }
        
        if cellModel.accesoryType == .checkmark{
            cell.accessoryType =  cellModel.isCellSelected ? cellModel.accesoryType : .none
        }
        else{
            cell.accessoryType = cellModel.accesoryType
        }
        
        return cell
    }
    
    //MARK:- Table selection
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return willSelect(at: indexPath, isDeselecting: false)
    }
    
    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        return willSelect(at: indexPath, isDeselecting: true)
    }
    
    private func willSelect(at indexPath: IndexPath, isDeselecting: Bool) -> IndexPath?{
        
        if let cellModel = getModel(for: indexPath){
            // Disable selection for non Selection cell type
            if cellModel.type == CellModelType.noSelection{
                return nil
            }
                // Disable selection for Accordion type cell type without selection
            else if cellModel.type.isAccordion{
                if openedAccordionModels.contains(cellModel){
                    self.accordionDeselection?(cellModel, indexPath)
                    openedAccordionModels = openedAccordionModels.filter{ $0 != cellModel}
                }
                else{
                    openedAccordionModels.append(cellModel)
                    self.accordionSelection?(cellModel, indexPath)
//                    return nil
                }
                if cellModel.type == CellModelType.accordion(false){
                    return nil
                }
            }
            else if cellModel.type == CellModelType.navigation{
                self.navigationSelection?(cellModel, indexPath)
                return nil
            }
        }
        return indexPath
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if let cellModel = getModel(for: indexPath), let cell = tableView.cellForRow(at: indexPath){
            cell.accessoryType = cellModel.accesoryType
            self.setSelectedData(indexPath: indexPath,newSelectionValue: true)
            self.selectedIndexPath?(indexPath)
        }
        
        if accordionToDeselect != nil{
            let parent = accordionToDeselect!.0
            let index = accordionToDeselect!.1
            accordionToDeselect = nil
            
            if let cellModel = getModel(for: indexPath), cellModel.parentModel?.first == parent{
                if let accordionCell = tableView.cellForRow(at: index) as? BaseTableViewCell{
                    accordionCell.setAccordionStatus(open: openedAccordionModels.contains(parent))
                }
                return
            }
            else{
                self.accordionDeselection?(parent, index)
                openedAccordionModels = openedAccordionModels.filter{ $0 != parent}
            }
            
            if let accordionCell = tableView.cellForRow(at: index) as? BaseTableViewCell{
                accordionCell.setAccordionStatus(open: openedAccordionModels.contains(parent))
            }
        }
    }
    
    var accordionToDeselect : (CellModel, IndexPath)?
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cellModel = getModel(for: indexPath), let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = cellModel.accesoryType == .checkmark ? .none : cellModel.accesoryType
            self.deselectedIndexPath?(indexPath)
            self.setSelectedData(indexPath: indexPath,newSelectionValue: false)
            
            if let parent = cellModel.parentModel?.first, openedAccordionModels.contains(parent), !allowsMultipleSelection,
                let index = getIndexPaths(for: .getSame([parent])).first{
                accordionToDeselect = (parent, index)
                
                if let accordionCell = tableView.cellForRow(at: indexPath) as? BaseTableViewCell{
                    accordionCell.setAccordionStatus(open: openedAccordionModels.contains(parent))
                    return
                }
            }
            else if openedAccordionModels.contains(cellModel), !allowsMultipleSelection{
                accordionToDeselect = (cellModel, indexPath)
            }
            
            if let accordionCell = tableView.cellForRow(at: indexPath) as? BaseTableViewCell{
                accordionCell.setAccordionStatus(open: openedAccordionModels.contains(cellModel))
            }
        }
    }
    
    //MARK:- Table Header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let tableSection = sections?[safe: section] , let headerIdentifier = tableSection.customHeaderXibIdentifier {
            let header  = Bundle.main.loadNibNamed(headerIdentifier, owner: nil, options: nil)![0] as? BaseTableHeaderView
            header?.config(model: tableSection)
            return header
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let tableSection = sections?[safe: section] else{ return 0 }
        if let height = tableSection.headerHeight{
            return height
        }
        return 44
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.sections?[section].title
    }
    
    //MARK:- Data source manage funciton
    func deselectCells(models: [CellModel]){
        let indexesToDeselect = getIndexPaths(for: .getSame(models))
        indexesToDeselect.forEach{
            deselectRow(at: $0, animated: true)
            tableView(self, didDeselectRowAt: $0)
        }
    }
    
    func setSelectedCells(){
        
        let selectedIndexes = getIndexPaths(for: .getSelected)
        selectedIndexes.forEach{
            selectRow(at: $0, animated: false, scrollPosition: .none)
            tableView(self, didSelectRowAt: $0)
        }
    }
    
    private func openAccordionCellOnStart(){
        
        let indexes = getIndexPaths(for: .getSelectedAndAccordion)
        indexes.forEach{
            _ = tableView(self, willSelectRowAt: $0)
        }
    }
    
    
    func insertCells(models:[CellModel], at index: IndexPath){
        
        var row = index.row
        let insertIndexes = models.map{ value -> IndexPath in
            row += 1
            return IndexPath(row: row, section: index.section)
        }
        
        if sections != nil{
            for (cellModel, index) in zip(models, insertIndexes){
              sections?[index.section].cells?.insert(cellModel, at: index.row)
            }
        }
        else{
            for (cellModel, index) in zip(models, insertIndexes){
                model.insert(cellModel, at: index.row)
            }
        }
        
        beginUpdates()
        if sections != nil{
            insertRows(at: insertIndexes, with: UITableView.RowAnimation.automatic)
        }
        else{
            insertRows(at: insertIndexes, with: UITableView.RowAnimation.automatic)
        }
        endUpdates()
        
        setSelectedCells()
    }
    
    func appendCell(models:[CellModel], in sectionIndex: Int? = nil){
        if sectionIndex != nil{
            sections?[sectionIndex!].cells?.append(contentsOf:models)
        }
        else{
            model.append(contentsOf: models)
        }
        
        beginUpdates()
        if sections != nil{
            let lastCell = sections?[sectionIndex!].cells?.count ?? 1
            insertRows(at: [IndexPath(row: lastCell - 1, section: sectionIndex!)], with: UITableView.RowAnimation.automatic)
        }
        else{
            insertRows(at: [IndexPath(row: model.count - 1, section: 0)], with: UITableView.RowAnimation.automatic)
        }
        endUpdates()
        
        setSelectedCells()
    }
    
    func updateCell(cellModel:CellModel){
        
        if let updateIndexPath = getIndexPaths(for: .updateSame(cellModel)).first{
            
            if sections != nil{
                self.sections![updateIndexPath.section].cells?[updateIndexPath.row] = cellModel
            }
            else{
                self.model[updateIndexPath.row] = cellModel
            }
            
            reloadRows(at: [updateIndexPath], with: .automatic)
        }
    }
    
    func deselectAllCells(){
        
        if sections != nil{
            var allCells:[CellModel] = []
            sections!.forEach { (model) in
                if let cells = model.cells {
                    allCells.append(contentsOf: cells)
                }
            }
            deselectCells(models: allCells)
        }else{
            deselectCells(models: model)
        }
    }
    
    func deleteCells(models:[CellModel]){
        
        var indexesToDelete = getIndexPaths(for: .getSame(models)).prefix(models.count)
        indexesToDelete.sort { (indexA, indexB) -> Bool in
            return (indexA.section == indexB.section && indexA.row > indexB.row)
        }
        
        if sections != nil{
            indexesToDelete.forEach { (indexPath) in
                sections![indexPath.section].cells?.remove(at: indexPath.row)
            }
        }
        else{
            indexesToDelete.forEach { (indexPath) in
                self.model.remove(at: indexPath.row)
            }
        }
        
        beginUpdates()
        deleteRows(at: Array(indexesToDelete), with: UITableView.RowAnimation.automatic)
        endUpdates()
        
        setSelectedCells()
    }
}


extension GenericTableView {
    
    enum IndexPathTypeOperation{
        case getSame([CellModel])
        case getSelected
        case getSelectedAndAccordion
        case getSameAndSelected([CellModel])
        case updateSame(CellModel)
    }
    
    private func getIndexPaths(for operation: IndexPathTypeOperation ) -> [IndexPath]{
        var indexPaths = [IndexPath]()
        if sections != nil{
            indexPaths = self.sections!.enumerated()
                .map{ [unowned self] (arg) -> [IndexPath] in
                    let (sectionIndex, section) = arg
                    return self.getIndexPathForCells(from: section.cells ?? [], in: sectionIndex, type: operation)
                }
                .flatMap { (sections) -> [IndexPath] in
                    return sections
            }
        }
        else{
            indexPaths = self.getIndexPathForCells(from: self.model, in: 0, type: operation)
        }
        return indexPaths
    }
    
    private func getIndexPathForCells(from sectionCells: [CellModel], in sectionIndex: Int, type: IndexPathTypeOperation) -> [IndexPath]{
        
        return sectionCells.enumerated().compactMap{ (row, cellModel) -> IndexPath? in
            var result:[CellModel] = []
            guard let data = cellModel.model else { return nil }
            
            switch type{
            case .getSame(let models):
                result = models.filter{$0.model?.getUniqueID() == data.getUniqueID()}
                break
            case .getSameAndSelected(let models):
                if cellModel.isCellSelected{
                    result = models.filter{$0.model?.getUniqueID() == data.getUniqueID()}
                }
                break
            case .getSelected:
                if cellModel.isCellSelected{
                    result = [cellModel]
                }
                break
                
            case .getSelectedAndAccordion:
                if cellModel.isCellSelected && cellModel.type.isAccordion{
                    result = [cellModel]
                }
            case .updateSame(let model):
                if cellModel.model?.getUniqueID() == model.model?.getUniqueID(){
                    result = [cellModel]
                }
            }
            
            if (result.count > 0 ){
                return IndexPath(row: row, section: sectionIndex)
            }
            return nil
        }
    }
    
    
    private func getModel(for indexPath: IndexPath) -> CellModel?{
        
        if sections != nil {
         
            if let section = sections![safe:indexPath.section]{
                return section.cells?[safe:indexPath.row]
            }
        }
        return model[safe: indexPath.row]
    }
    
    private func setSelectedData(indexPath: IndexPath, newSelectionValue: Bool){
        
        if sections != nil {
            
            sections![indexPath.section].cells?[indexPath.row].isCellSelected = newSelectionValue
            let lastSelected = newSelectionValue ? sections![indexPath.section].cells?[indexPath.row] : nil
            self.selectedData?(sections!.flatMap({ (section) -> [CellModel] in
                return section.cells!.filter{$0.isCellSelected}
            }), self.indexPathsForSelectedRows, lastSelected)
        }
        else{
            model[indexPath.row].isCellSelected = newSelectionValue
            let lastSelected = newSelectionValue ? model[indexPath.row] : nil
            self.selectedData?(model.filter{$0.isCellSelected}, self.indexPathsForSelectedRows, lastSelected)
        }
    }
    
    private func getSelectedData() -> [CellModel]{
        if sections != nil{
            return sections!.flatMap({ (section) -> [CellModel] in
                return section.cells!.filter{$0.isCellSelected}
            })
        }
        else{
            return model.filter{$0.isCellSelected}
        }
    }
}

extension GenericTableView : BaseCellSelectionIndicator{
    func selectionIndicatorAction(cell: BaseTableViewCell, isSelected: Bool) {
        if let index = indexPath(for: cell){
            
            // Deselect previous cell for single selection
            if !allowsMultipleSelection{
                if let indexes = self.indexPathsForSelectedRows{
                    indexes.forEach{
                        deselectRow(at: $0 , animated: false)
                        tableView(self, didDeselectRowAt: $0)
                    }
                }
            }
            
            if isSelected{
                selectRow(at: index, animated: false, scrollPosition: .none)
                tableView(self, didSelectRowAt: index)
            }
            else{
                deselectRow(at: index, animated: false)
                tableView(self, didDeselectRowAt: index)
            }
            
            self.setSelectedData(indexPath: index, newSelectionValue: isSelected)
            setSelectedCells()
        }
    }
}

//ERROR: Set this in more propriapte place
extension Collection {
    
    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
