//
//  BaseTableViewCell.swift
//  modelhubapp_ios
//
//  Created by admin on 1/28/19.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit

protocol BaseCellSelectionIndicator : class{
    func selectionIndicatorAction(cell: BaseTableViewCell, isSelected: Bool)
}

class BaseTableViewCell: UITableViewCell {

    weak var delegate: BaseCellSelectionIndicator?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }
    
    //Required to implement in subclass
    func config(data:CellModelProtocol?){
        fatalError("Must override")
    }
    
    func cellIsSelected(isSelected: Bool){
        fatalError("Must override")
    }
    
    public func setAccordionStatus(open: Bool){
        print("is open \(open)")
    }
    
    // Required to implement to set selection from subclass from specific UI elements from cell
    func setModelSelected(isSelected: Bool){
        self.delegate?.selectionIndicatorAction(cell: self, isSelected: isSelected)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        cellIsSelected(isSelected: selected)
    }
    

}
