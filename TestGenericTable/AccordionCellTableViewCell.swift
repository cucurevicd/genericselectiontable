//
//  AccordionCellTableViewCell.swift
//  TestGenericTable
//
//  Created by Apple on 2/20/19.
//  Copyright © 2019 Quantox. All rights reserved.
//

import UIKit

class AccordionCellTableViewCell: BaseTableViewCell {

    @IBOutlet weak var accordionOpenIndicatorLabel: UILabel!
    var opendAccordion: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func config(data: CellModelProtocol?) {
        
    }
    
    override func cellIsSelected(isSelected: Bool) {
            self.backgroundColor = isSelected ? .yellow : .white
            self.accordionOpenIndicatorLabel.text = isSelected  ? "Selected" : "Not selected"
    }

    override func setAccordionStatus(open: Bool) {
        self.opendAccordion = open
        self.backgroundColor = open ? .yellow : .white
        self.accordionOpenIndicatorLabel.text = open ? "Selected" : "Not selected"
    }
}
