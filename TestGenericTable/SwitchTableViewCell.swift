//
//  SwitchTableViewCell.swift
//  TestGenericTable
//
//  Created by Apple on 1/31/19.
//  Copyright © 2019 Quantox. All rights reserved.
//

import UIKit

class SwitchTableViewCell: BaseTableViewCell {

    @IBOutlet weak var switchIndicator: UISwitch!
    @IBOutlet weak var indicatorLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func switchAction(_ sender: UISwitch) {
        setModelSelected(isSelected: sender.isOn)
    }
    
    //MARK:- Override superclass methods
    override func config(data: CellModelProtocol?) {
        
    }
    
    override func cellIsSelected(isSelected: Bool) {
        indicatorLabel.isHidden = !isSelected
        switchIndicator.setOn(isSelected, animated: true)
    }
}
