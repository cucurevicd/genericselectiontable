//
//  ViewController.swift
//  TestGenericTable
//
//  Created by Apple on 1/30/19.
//  Copyright © 2019 Quantox. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    lazy var cell1 = CellModel(identifier: "SwitchTableViewCell", model: basicCell, type: .selection, isCellSelected: false, accessoryType: .checkmark)
    lazy var cell2 = CellModel(identifier: "basicSubtitleCell", model: subtitleBasicCell, type: .selection, accessoryType: .checkmark)
    lazy var cell3 = CellModel(identifier: "basicCell", model: basicCell, type: .accordion(true),isCellSelected: true, accessoryType: .checkmark)
    lazy var cell4 = CellModel(identifier: "basicCell", model: basicCell, type: .noSelection, accessoryType: .checkmark)
    lazy var cell5 = CellModel(identifier: "basicSubtitleCell", model: subtitleBasicCell, type: .navigation, accessoryType: .disclosureIndicator)
    
    var basicCell: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Hello")
    }
    var subtitleBasicCell : NativeBasicSubtitleCell{
        return NativeBasicSubtitleCell(title: "Subtitle", subtitle: "Hello subtitle")
    }
    
    var accordionModel1: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Accordion1")
    }
    
    var accordionModel2: NativeBasicTableCell{
        return NativeBasicTableCell(title: "Accordion2")
    }
    
    var embeded: GenericTableController? {
        return self.children.filter({ (value) -> Bool in return value.isKind(of: GenericTableController.self)}).first as? GenericTableController
    }

    //MARK:- View cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        // Select one of scenarions
        selectingMultipleOptionsWithSingleAccordionOption()
        
        // Call this after config table to set all cells to selected/not selected
        embeded?.tableView.setSelectedCells()
    }
    
    //MARK:- Cells
    func getCells() -> [CellModel]{
        return [cell1, cell2, cell3, cell4, cell5]
    }
    
    lazy var additionalCells : [CellModel] = {
        let cell1 = CellModel(identifier: "SubAccordionTableViewCell", model: accordionModel1, type: .selection, isCellSelected: false, accessoryType: .checkmark, parent: cell3)
        let cell2 = CellModel(identifier: "SubAccordionTableViewCell", model: accordionModel2, type: .selection,isCellSelected: false, accessoryType: .none, parent: cell3)
        return [cell1, cell2]
    }()

    //MARK:- Versions
    private func normalSingleSelection(){
        
        embeded?.tableView.accordionSelection = { [unowned self] (model, indexPath) in
            print("Accordion add:")
            print("IndexPath:\(indexPath)")
            self.embeded?.tableView.insertCells(models: self.additionalCells,at: indexPath)
        }
        
        embeded?.tableView.accordionDeselection = { [unowned self] (model, indexPath) in
            print("Accordion remove:")
            print("IndexPath:\(indexPath)")
            self.embeded?.tableView.deleteCells(models: self.additionalCells)
        }
        
        embeded!.tableView.config(data: getCells(), isMultipleSelection: false) { [unowned self] (selectedCell, selectedIndexPaths, lastSelected) in
            print("Selected titles: \(selectedCell.map{$0.model?.getTitle()})")
        }
    }
    
    private func selectingMultipleOptionsWithSingleAccordionOption(){
        
        embeded?.tableView.selectedIndexPath = { indexPath in
            print("Last selected indexpath: \(indexPath)")
        }
        
        embeded?.tableView.deselectedIndexPath = { indexPath in
            print("Last deselected indexpath: \(indexPath)")
        }
        
        embeded?.tableView.navigationSelection = { [unowned self] (model, indexPath) in
            print("Segue preform:")
            print(model.model?.getTitle())
            print(indexPath)
            self.performSegue(withIdentifier: "secondViewController", sender: nil)
        }
        
        embeded?.tableView.accordionSelection = { [unowned self] (model, indexPath) in
            print("Accordion add:")
            print("IndexPath:\(indexPath)")
            self.embeded?.tableView.insertCells(models: self.additionalCells,at: indexPath)
        }
        
        embeded?.tableView.accordionDeselection = { [unowned self] (model, indexPath) in
            print("Accordion remove:")
            print("IndexPath:\(indexPath)")
            self.embeded?.tableView.deleteCells(models: self.additionalCells)
        }
        
        embeded!.tableView.config(data: getCells(), isMultipleSelection: true) { (selectedCell, selectedIndexPaths, lastSelected) in
            
            print("Selected titles: \(selectedCell.map{$0.model?.getTitle()})")
            
            // Remove previously selected cell in accordion sub types
            if let lastSelection = lastSelected{
                if let parent = lastSelection.parentModel{
                    let newSelection = selectedCell.filter{ $0 != lastSelection }
                    let deselectModels = newSelection.filter{ $0.parentModel?.first == parent.first && $0 != lastSelection }
                    self.embeded?.tableView.deselectCells(models: deselectModels)
                }
            }
        }
    }

}

