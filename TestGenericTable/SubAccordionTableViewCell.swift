//
//  SubAccordionTableViewCell.swift
//  TestGenericTable
//
//  Created by Apple on 1/31/19.
//  Copyright © 2019 Quantox. All rights reserved.
//

import UIKit

class SubAccordionTableViewCell: BaseTableViewCell {
    
    @IBOutlet weak var titleLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func config(data: CellModelProtocol?) {
        
        self.titleLabel.text = data?.getTitle()
    }
    
    override func cellIsSelected(isSelected: Bool) {    
        self.backgroundColor = isSelected ? .red : .blue
    }
}
